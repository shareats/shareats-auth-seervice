package master.ccm2.shareatsauth.messages;

/**
 * enum qui contient tous les message d'erreur renvoyés par l'api
 * @author oem
 *
 */
public enum ErrorMessage{
	PASSWORD_TO_SHORT("Le mot de passe doit contenir 6 caractères au minimum"),
	EMAIL_NOT_VALID("Un email valide est requis pour l'inscription"),
	ERREUR_FORM_LOGIN("Mot de passe ou email incorrect"),
	TOKEN_NOT_VALID("Token invalide"),
	TOKEN_REVOKE_ERROR("Erreur l'orsque on revoke le token"),
	BUCKET_DELETE_ERROR("Erreur lors de la suppression du fichier"),
	USER_PHOTO_ERROR("La photo envoyée ne correspond pas à la photo de profil"),
	EMAIL_ALREADY_EXIST("Un compte existe déja pour cette utilisateur"),
	ERREUR_DISPLAYNAME("Le nom n'est pas conforme");
	
	private final String messagErreur;

	
	
	private ErrorMessage(String messagErreur) {
		this.messagErreur = messagErreur;
	}


	public String getMessageErreur() {
		return messagErreur;
	}
	
	
	

}
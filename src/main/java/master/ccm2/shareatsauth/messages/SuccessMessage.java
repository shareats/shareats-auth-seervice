package master.ccm2.shareatsauth.messages;

public enum SuccessMessage {
	
	USER_ADD_SUCCESS("L'utilisateur a été ajouté avec succès"),
	USER_UPDATE_SUCCESS("L'utilisateur a été mise à jour avec succès"),
	TOKEN_REVOKE_SUCCESS("Le token a été revoké avec succès"),
	USER_DELETE_SUCCESS("L'utilisateur a été supprimer avec succès"),
	USER_LOGIN_SUCCESS("L'utilisateur a été connecté avec succès"),
	BUCKET_DELETE_SUCCESS("Le fichier a été supprimé"),
	BUCKET_UPLOAD_SUCCESS("Le fichier a été enregistré avec succès"),
	BUCKET_DOWNLOAD_SUCCESS("Le fichier a été télécharger avec succès"),
	USER_PHOTO_SUCCESS("La photo de profil a été vérifié avec succès"),
	USER_GET_SUCCESS("Infos de l'utilisateur avec succès");
	
	private final String messageSuccess;

	
	
	private SuccessMessage(String messagErreur) {
		this.messageSuccess = messagErreur;
	}

	

	public String getMessageSuccess() {
		return messageSuccess;
	}
	
}

package master.ccm2.shareatsauth.exceptions;

import master.ccm2.shareatsauth.messages.ErrorMessage;

public class ErreurAuthException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String erreurMessage;
	
	
	public ErreurAuthException(ErrorMessage errorMessage) {
		super();
		this.erreurMessage = errorMessage.getMessageErreur();
	}
	
	public String getErreurMessage() {
		return erreurMessage;
	}
	public void setErreurMessage(String erreurMessage) {
		this.erreurMessage = erreurMessage;
	}

}

package master.ccm2.shareatsauth.exceptions;

import master.ccm2.shareatsauth.messages.ErrorMessage;

/**
 * exception renvoyés lorsque il y'a une erreur
 * des erreurs sur les infos envoyés par le client
 * @author oem
 *
 */
public class ErreurManageUserException extends Exception {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String erreurMessage;

	public ErreurManageUserException(ErrorMessage errorMessageClass) {
		super();
		this.setErreurMessage(errorMessageClass.getMessageErreur());
	}

	public String getErreurMessage() {
		return erreurMessage;
	}

	public void setErreurMessage(String erreurMessage) {
		this.erreurMessage = erreurMessage;
	}
	
	
}

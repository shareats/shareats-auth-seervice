package master.ccm2.shareatsauth.controllers;

import java.util.ArrayList;
import java.util.Map;
import java.util.concurrent.ExecutionException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.google.cloud.storage.StorageException;
import com.google.firebase.auth.FirebaseAuthException;
import com.google.firebase.auth.FirebaseToken;

import master.ccm2.shareatsauth.exceptions.ErreurAuthException;
import master.ccm2.shareatsauth.interfaces.AuthInterface;
import master.ccm2.shareatsauth.interfaces.DataBaseInterface;
import master.ccm2.shareatsauth.messages.SuccessMessage;
import master.ccm2.shareatsauth.models.MonBucket;
import master.ccm2.shareatsauth.models.ResponseApi;
import master.ccm2.shareatsauth.models.Roles;
@RestController
public class RoleController {
	@Autowired 
	AuthInterface authService;
	
    @Autowired
    DataBaseInterface databaseService;
    
    /**
     * Récupère les roles enregistrés dans firebase
     */
    @RequestMapping(value="/roles/{token}", method= RequestMethod.GET)
    public ResponseEntity<Roles> getRoles(@PathVariable String token){
    	Roles roles =new Roles();
    	try {
			authService.autentification(token);
			Map<String, Object> result =databaseService.get("roles", "lesRoles");
			if(result!=null) {
				roles.setRoles((ArrayList<String>) result.get("roles"));
			}
		} catch (FirebaseAuthException e) {
			e.printStackTrace();
			return new ResponseEntity<Roles>(roles, HttpStatus.BAD_REQUEST);
		} catch (ErreurAuthException e) {
			e.printStackTrace();
			return new ResponseEntity<Roles>(roles, HttpStatus.BAD_REQUEST);

		} catch (InterruptedException e) {
			e.printStackTrace();
			return new ResponseEntity<Roles>(roles, HttpStatus.BAD_REQUEST);
		} catch (ExecutionException e) {
			e.printStackTrace();
			return new ResponseEntity<Roles>(roles, HttpStatus.BAD_REQUEST);
		}
    	
	 return new ResponseEntity<Roles>(roles, HttpStatus.OK);
    	
    }
    	
}

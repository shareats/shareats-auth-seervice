package master.ccm2.shareatsauth.controllers;

import java.util.concurrent.ExecutionException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.google.firebase.auth.FirebaseAuthException;
import com.google.firebase.auth.FirebaseToken;

import master.ccm2.shareatsauth.exceptions.ErreurAuthException;
import master.ccm2.shareatsauth.interfaces.AuthInterface;
import master.ccm2.shareatsauth.interfaces.ManageUserInterface;
import master.ccm2.shareatsauth.messages.ErrorMessage;
import master.ccm2.shareatsauth.messages.SuccessMessage;
import master.ccm2.shareatsauth.models.ResponseApi;
import master.ccm2.shareatsauth.models.User;


/**
 * défini les routes de l'api pour l'authentification ,la déconnexion ,mise à jour d'un mot de passe d'un utilisateur
 */
@RestController
public class AuthController {
	
	// TODO ajouter class exception pour l'authentification
	
	@Autowired 
	AuthInterface authService;
    @Autowired
    ManageUserInterface ManageUserService;
	/***
	 * vérifie le token envoyé par le client est bon et renvoie l'id du user
	 * @param idToken le token envoyé par le client
	 * @return true si le token est bon
	 * @return false return false si token est pas bon
	 */
    @RequestMapping(value="/api/login/{token}", method= RequestMethod.POST)
    public ResponseEntity<ResponseApi> login(@PathVariable String token){
    	ResponseApi reponse = new ResponseApi();
    	FirebaseToken userInfoToken = null;
    	User user=null;
    	try {
    		userInfoToken=authService.autentification(token);
    		try {
				user=ManageUserService.getByUid(userInfoToken.getUid());
			} catch (InterruptedException e) {
				reponse.setMessage(e.getMessage());
	        	reponse.setResponse(userInfoToken);
				e.printStackTrace();
			} catch (ExecutionException e) {
				reponse.setMessage(e.getMessage());
	        	reponse.setResponse(userInfoToken);
				e.printStackTrace();
			}
        } catch (FirebaseAuthException e) {
        	reponse.setMessage(e.getMessage());
        	reponse.setResponse(userInfoToken);
        	return new ResponseEntity<ResponseApi>(reponse,HttpStatus.BAD_REQUEST);
 
        } catch (ErreurAuthException e) {
        	reponse.setMessage(e.getErreurMessage());
        	reponse.setResponse(userInfoToken);
        	return new ResponseEntity<ResponseApi>(reponse,HttpStatus.BAD_REQUEST);
		}
    	reponse.setMessage(SuccessMessage.USER_LOGIN_SUCCESS.getMessageSuccess());
    	reponse.setResponse(user);
		return new ResponseEntity<ResponseApi>(reponse,HttpStatus.OK);
    }
    
    /**
     * déconnecte le user en révokan son token 
     * @param idToken idToken le token envoyé par le client
     * @return uid l'id du user connecté
     */
    @RequestMapping(value="/api/disconnect/{UserUid}", method= RequestMethod.POST)
    public ResponseEntity<ResponseApi> disconnect(@PathVariable String UserUid){
    	ResponseApi reponse = new ResponseApi();

        try {
            try {
            	authService.revokeToken(UserUid);
			} catch (ErreurAuthException e) {
				reponse.setMessage(e.getErreurMessage());
				return new ResponseEntity<ResponseApi>(reponse,HttpStatus.BAD_REQUEST);
			}
        } catch (FirebaseAuthException e) {
			reponse.setMessage(e.getMessage());

        	return new ResponseEntity<ResponseApi>(reponse,HttpStatus.BAD_REQUEST);
        }
		reponse.setMessage(SuccessMessage.TOKEN_REVOKE_SUCCESS.getMessageSuccess());

		return new ResponseEntity<ResponseApi>(reponse,HttpStatus.OK);
    }

}

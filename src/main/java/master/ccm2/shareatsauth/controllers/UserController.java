package master.ccm2.shareatsauth.controllers;

import com.google.firebase.auth.FirebaseAuthException;
import com.google.firebase.auth.FirebaseToken;

import master.ccm2.shareatsauth.exceptions.ErreurAuthException;
import master.ccm2.shareatsauth.exceptions.ErreurManageUserException;
import master.ccm2.shareatsauth.interfaces.AuthInterface;
import master.ccm2.shareatsauth.interfaces.ManageUserInterface;
import master.ccm2.shareatsauth.messages.ErrorMessage;
import master.ccm2.shareatsauth.messages.SuccessMessage;
import master.ccm2.shareatsauth.models.ResponseApi;
import master.ccm2.shareatsauth.models.User;

import java.io.IOException;
import java.net.MalformedURLException;
import java.util.concurrent.ExecutionException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

/**
 * définit les routes de l'api pour ajouter ,supprimer , mettre à jour un utilisateur
 */
@RestController
public class UserController {

    @Autowired
    ManageUserInterface ManageUserService;
    @Autowired 
    AuthInterface authService;

    public UserController() {
    }

    /**
     * ajoute un utilisateur
     * @param newUser les données de l'utlisateur à crée
     * @return
     */
    @RequestMapping(value="/users/create", method= RequestMethod.POST)
    public ResponseEntity<ResponseApi> create(@RequestBody User newUser){
        ResponseApi reponse = new ResponseApi();

        try {
            try {
                ManageUserService.create(newUser);
            } catch (ErreurManageUserException e) {
                reponse.setMessage(e.getErreurMessage());
                return new ResponseEntity<ResponseApi>(reponse,HttpStatus.BAD_REQUEST);
            }
        } catch (FirebaseAuthException e) {
            reponse.setMessage(e.getMessage());

            return new ResponseEntity<ResponseApi>(reponse,HttpStatus.BAD_REQUEST);
           
        }
        reponse.setMessage(SuccessMessage.USER_ADD_SUCCESS.getMessageSuccess());

        return new ResponseEntity<ResponseApi>(reponse,HttpStatus.CREATED);
    }

    /**
     * met à jour un utilisateur
     * @param token le  token du client
     */
    @RequestMapping(value="/users/update/{token}", method= RequestMethod.PUT)
    public  ResponseEntity<ResponseApi>  update(@PathVariable String token, @RequestBody User userData){
        ResponseApi reponse = new ResponseApi();

        FirebaseToken userInToken = null;
        User user=null;
        try {
            //vérifie le token  du client
            userInToken = authService.autentification(token);
        
            //met à jour l'utilisateur
            user =  ManageUserService.update(userInToken,userData);
        } catch (FirebaseAuthException e) {
            reponse.setMessage(e.getMessage());
            return new ResponseEntity<ResponseApi>(reponse,HttpStatus.BAD_REQUEST);
        } catch (ErreurManageUserException e) {
            reponse.setMessage(e.getErreurMessage());
            return new ResponseEntity<ResponseApi>(reponse,HttpStatus.BAD_REQUEST);
        } catch (ErreurAuthException e) {
            reponse.setMessage(e.getErreurMessage());
            return new ResponseEntity<ResponseApi>(reponse,HttpStatus.BAD_REQUEST);
        }
        reponse.setMessage(SuccessMessage.USER_UPDATE_SUCCESS.getMessageSuccess());
        reponse.setResponse(user);
        return new ResponseEntity<ResponseApi>(reponse,HttpStatus.OK);
    }

    /**
     * supprime un utilisateur
     * @param  token le  token du client
     */
    @RequestMapping(value="/users/delete/{token}", method= RequestMethod.DELETE)
    public ResponseEntity<String> delete(@PathVariable String token) {
        FirebaseToken userInToken = null;
        ResponseApi reponse = new ResponseApi();

        try {
            //vérifie le token  du client
            userInToken=authService.autentification(token);
             ManageUserService.delete(userInToken.getUid());
        } catch (FirebaseAuthException e) {
            reponse.setMessage(e.getMessage());
            return new ResponseEntity<String>(e.getMessage(),HttpStatus.BAD_REQUEST);

        } catch (ErreurAuthException e) {
            reponse.setMessage(e.getErreurMessage());
            return new ResponseEntity<String>(e.getErreurMessage(),HttpStatus.BAD_REQUEST);

        }
        reponse.setMessage(SuccessMessage.USER_DELETE_SUCCESS.getMessageSuccess());
        return new ResponseEntity<String>(SuccessMessage.USER_DELETE_SUCCESS.getMessageSuccess(),HttpStatus.OK);
    }

    /**
     * récupère les infos d'un seul user
     * @param id
     */
    @RequestMapping(value="/users/{token}", method= RequestMethod.GET)
    public ResponseEntity<ResponseApi>  get(@PathVariable String token){
        ResponseApi reponse = new ResponseApi();
        FirebaseToken userInToken = null;
        User user = null;
        try {
            //vérifie le token  du client
            userInToken = authService.autentification(token);
            user=ManageUserService.getByUid(userInToken.getUid());
        } catch (FirebaseAuthException e) {
            reponse.setMessage(e.getMessage());
            e.printStackTrace();
            return new ResponseEntity<ResponseApi>(reponse,HttpStatus.BAD_REQUEST);

        } catch (ErreurAuthException e) {
            reponse.setMessage(e.getErreurMessage());
            return new ResponseEntity<ResponseApi>(reponse,HttpStatus.BAD_REQUEST);

        } catch (InterruptedException e) {
            reponse.setMessage(e.getMessage());
            e.printStackTrace();
            return new ResponseEntity<ResponseApi>(reponse,HttpStatus.BAD_REQUEST);
        } catch (ExecutionException e) {
            reponse.setMessage(e.getMessage());
            e.printStackTrace();
            return new ResponseEntity<ResponseApi>(reponse,HttpStatus.BAD_REQUEST);
        }
        reponse.setMessage(SuccessMessage.USER_GET_SUCCESS.getMessageSuccess());
        reponse.setResponse(user);
        return new ResponseEntity<ResponseApi>(reponse,HttpStatus.OK);
    }
    
    @RequestMapping(value="/user/{id}", method= RequestMethod.GET)
    public ResponseEntity<Object>  getUserById(@PathVariable String id){
        User user = null;
        try {
            user=ManageUserService.getByUid(id);
        } catch (FirebaseAuthException e) {
            e.printStackTrace();
            return new ResponseEntity<Object>(e.getMessage(),HttpStatus.BAD_REQUEST);

        } catch (InterruptedException e) {
            e.printStackTrace();
            return new ResponseEntity<Object>(e.getMessage(),HttpStatus.BAD_REQUEST);
        } catch (ExecutionException e) {
            e.printStackTrace();
            return new ResponseEntity<Object>(e.getMessage(),HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<Object>(user,HttpStatus.OK);
    }

    /**
     * récupère les infos d'un seul user
     * @param id
     */
    @RequestMapping(value="/users/checkphoto/{token}", method= RequestMethod.POST)
    public ResponseEntity<ResponseApi>  checkphoto(@PathVariable String token,@RequestParam MultipartFile photoAverifier){
        ResponseApi reponse = new ResponseApi();
        Boolean responseIa =null;
        try {
            FirebaseToken userInToken = authService.autentification(token);
             responseIa = ManageUserService.checkUserPhoto(userInToken, photoAverifier);
            if(!responseIa) {
                reponse.setResponse(responseIa);
                reponse.setMessage(ErrorMessage.USER_PHOTO_ERROR.getMessageErreur());
                return new ResponseEntity<ResponseApi>(reponse,HttpStatus.BAD_REQUEST);
            }
        } catch (FirebaseAuthException e) {
            //reponse.setMessage(e.getMessage());
            reponse.setMessage("FirebaseAuthException");
            return new ResponseEntity<ResponseApi>(reponse,HttpStatus.BAD_REQUEST);
        } catch (ErreurAuthException e) {
            reponse.setMessage(e.getErreurMessage());
            return new ResponseEntity<ResponseApi>(reponse,HttpStatus.BAD_REQUEST);
        } catch (MalformedURLException e) {
            //reponse.setMessage(e.getMessage());
            reponse.setMessage("MalformedURLException");
            return new ResponseEntity<ResponseApi>(reponse,HttpStatus.BAD_REQUEST);
        } catch (IOException e) {
            //reponse.setMessage(e.getMessage());
            reponse.setMessage("IOException");
            return new ResponseEntity<ResponseApi>(reponse,HttpStatus.BAD_REQUEST);
        }
        reponse.setResponse(responseIa);
        reponse.setMessage(SuccessMessage.USER_PHOTO_SUCCESS.getMessageSuccess());
        return new ResponseEntity<ResponseApi>(reponse,HttpStatus.BAD_REQUEST);
    }
    
}

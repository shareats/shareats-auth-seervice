package master.ccm2.shareatsauth.controllers;

import java.io.File;
import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.google.cloud.storage.StorageException;
import com.google.firebase.auth.FirebaseAuthException;
import com.google.firebase.auth.FirebaseToken;

import master.ccm2.shareatsauth.exceptions.ErreurAuthException;
import master.ccm2.shareatsauth.interfaces.AuthInterface;
import master.ccm2.shareatsauth.messages.ErrorMessage;
import master.ccm2.shareatsauth.messages.SuccessMessage;
import master.ccm2.shareatsauth.models.MonBucket;
import master.ccm2.shareatsauth.models.ResponseApi;
import master.ccm2.shareatsauth.services.BucketStorageService;
/**
 * définit les routes pour télécharger , uploader , récupérer du bucket
 * @author zak
 *
 */

@RestController
public class BucketController {
	@Autowired 
	AuthInterface authService;
	@Autowired
	BucketStorageService bucketService;
	/**
	 * supprime un fichier du bucket
	 * @param idToken
	 * @param bucket 
	 * @return
	 */
    @RequestMapping(value="/bucket/delete/{token}", method= RequestMethod.POST)
    public ResponseEntity<ResponseApi> delete(@PathVariable String token,@RequestBody MonBucket bucket){
    	ResponseApi reponse = new ResponseApi();
    	FirebaseToken userInfoToken = null;
    	try {
    		userInfoToken=authService.autentification(token);
    		Boolean result =bucketService.deleteFile(bucket.getFileName());
    		if(!result) {
    			reponse.setMessage(ErrorMessage.BUCKET_DELETE_ERROR.getMessageErreur());
    			return new ResponseEntity<ResponseApi>(reponse,HttpStatus.BAD_REQUEST);
    		}
        } catch (FirebaseAuthException e) {
        	reponse.setMessage(e.getMessage());
        	return new ResponseEntity<ResponseApi>(reponse,HttpStatus.BAD_REQUEST);
 
        } catch (ErreurAuthException e) {
        	reponse.setMessage(e.getErreurMessage());
        	return new ResponseEntity<ResponseApi>(reponse,HttpStatus.BAD_REQUEST);
		}
    	reponse.setMessage(SuccessMessage.BUCKET_DELETE_SUCCESS.getMessageSuccess());
		return new ResponseEntity<ResponseApi>(reponse,HttpStatus.OK);
    }
    /**
     * téléverse le fichier dans le bucket
     * @param idToken
     * @param bucket
     * @return le lien du fichier télécharger dans le bucket
     */
    @RequestMapping(value="/bucket/upload/{token}", method= RequestMethod.POST)
    public ResponseEntity<ResponseApi> upload(@PathVariable String token,@RequestParam MultipartFile file,
    		@RequestParam String fileName){
    	ResponseApi reponse = new ResponseApi();
    	FirebaseToken userInfoToken = null;
    	String urlFichier =null;
    	
    	try {
    		userInfoToken=authService.autentification(token);
    		urlFichier = bucketService.uploadFile(fileName, file);
        } catch (FirebaseAuthException e) {
        	reponse.setMessage(e.getMessage());
        	return new ResponseEntity<ResponseApi>(reponse,HttpStatus.BAD_REQUEST);
        } catch (ErreurAuthException e) {
        	reponse.setMessage(e.getErreurMessage());
        	return new ResponseEntity<ResponseApi>(reponse,HttpStatus.BAD_REQUEST);
		} catch (StorageException e) {
			reponse.setMessage(e.getMessage());
        	return new ResponseEntity<ResponseApi>(reponse,HttpStatus.BAD_REQUEST);
		} catch (IOException e) {
			reponse.setMessage(e.getMessage());
        	return new ResponseEntity<ResponseApi>(reponse,HttpStatus.BAD_REQUEST);
		}
    	
    	reponse.setMessage(SuccessMessage.BUCKET_UPLOAD_SUCCESS.getMessageSuccess());
    	reponse.setResponse(urlFichier);
    	
		return new ResponseEntity<ResponseApi>(reponse,HttpStatus.OK);
    }
    /**
     * télécharge le fichier du bucket
     * @param idToken
     * @param bucket
     * @return
     */
    @RequestMapping(value="/bucket/download/{token}", method= RequestMethod.GET)
    public ResponseEntity<ResponseApi> Download(@PathVariable String token,@RequestBody MonBucket bucket){
    	ResponseApi reponse = new ResponseApi();
    	FirebaseToken userInfoToken = null;
    	String fichierDownload;
    	try {
    		userInfoToken=authService.autentification(token);
    		fichierDownload=bucketService.DownloadFile(bucket.getFileName());
        } catch (FirebaseAuthException e) {
        	reponse.setMessage(e.getMessage());
        	return new ResponseEntity<ResponseApi>(reponse,HttpStatus.BAD_REQUEST);
        } catch (ErreurAuthException e) {
        	reponse.setMessage(e.getErreurMessage());
        	return new ResponseEntity<ResponseApi>(reponse,HttpStatus.BAD_REQUEST);
		} catch (StorageException e) {
			reponse.setMessage(e.getMessage());
        	return new ResponseEntity<ResponseApi>(reponse,HttpStatus.BAD_REQUEST);
		}
    	reponse.setResponse(fichierDownload);
    	reponse.setMessage(SuccessMessage.BUCKET_DOWNLOAD_SUCCESS.getMessageSuccess());
		return new ResponseEntity<ResponseApi>(reponse,HttpStatus.OK);
    }
    
}

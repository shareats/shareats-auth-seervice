package master.ccm2.shareatsauth.models;

import org.springframework.web.multipart.MultipartFile;

public class MonBucket {
	private String bucketName;
	private String fileName;
	private MultipartFile file;
	public MonBucket(String bucketName, String fileName, MultipartFile file) {
		super();
		this.setBucketName(bucketName);
		this.setFileName(fileName);
		this.setFile(file);
	}

	public MonBucket() {
		super();
	}

	public String getBucketName() {
		return bucketName;
	}

	public void setBucketName(String bucketName) {
		this.bucketName = bucketName;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public MultipartFile getFile() {
		return file;
	}

	public void setFile(MultipartFile file) {
		this.file = file;
	}
}

package master.ccm2.shareatsauth.models;

import java.util.Map;

/**
 * Un utilisateur de l'application est définit par son:
 * nom ,mail ,mot de passe,numéro téléphone,prénom.
 */

public class User {

    private  String uid;
    private  String email;
    private  String password;
    private String phoneNumber;
    private String displayName;
    private  String urlPhoto;
    private  boolean isPhotoVerified;
    private Map<String, Object> claims;
    public User() {
    	super();
    }
    public User(String uid, String email, String phoneNumber, String displayName, String urlPhoto,boolean isPhotoVerified, Map<String, Object> claims) {
    	super();
        this.uid = uid;
        this.email = email;
        this.phoneNumber = phoneNumber;
        this.displayName = displayName;
        this.urlPhoto = urlPhoto;
        this.isPhotoVerified=isPhotoVerified;
        this.claims=claims;
    }
    
    

    public User(String uid, String email, String password, String phoneNumber, String displayName, String urlPhoto) {
		super();
		this.uid = uid;
		this.email = email;
		this.password = password;
		this.phoneNumber = phoneNumber;
		this.displayName = displayName;
		this.urlPhoto = urlPhoto;
	}



	public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String lastName) {
        this.displayName = lastName;
    }



    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getUid() {
        return uid;
    }

    public void setUrlPhoto(String urlPhoto) {
        this.urlPhoto = urlPhoto;
    }

    public String getUrlPhoto() {
        return urlPhoto;
    }
	public boolean isPhotoVerified() {
		return isPhotoVerified;
	}
	public void setPhotoVerified(boolean isPhotoVerified) {
		this.isPhotoVerified = isPhotoVerified;
	}
	public Map<String, Object> getClaims() {
		return claims;
	}
	public void setClaims(Map<String, Object> claims) {
		this.claims = claims;
	}
}

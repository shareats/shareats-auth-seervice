package master.ccm2.shareatsauth.models;

public class ResponseApi {
	private String message;
	private Object response;
	public ResponseApi() {
		super();

	}

	public ResponseApi(String message, Object response) {
		super();
		this.setMessage(message);
		this.setResponse(response);
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public Object getResponse() {
		return response;
	}

	public void setResponse(Object response) {
		this.response = response;
	}

}

package master.ccm2.shareatsauth.models;

import java.util.ArrayList;

public class Roles {
	private ArrayList<String> roles;
	
	public Roles() {
		
	}

	public Roles(ArrayList<String> roles) {
		super();
		this.roles = roles;
	}

	public  ArrayList<String> getRoles() {
		return roles;
	}

	public void setRoles(ArrayList<String> roles) {
		this.roles = roles;
	}
	
}

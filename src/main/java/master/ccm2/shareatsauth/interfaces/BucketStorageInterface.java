package master.ccm2.shareatsauth.interfaces;

import java.io.File;
import java.io.IOException;

import org.springframework.web.multipart.MultipartFile;

import com.google.cloud.storage.Blob;
import com.google.cloud.storage.Bucket;
import com.google.cloud.storage.StorageException;

public interface BucketStorageInterface {
	public Boolean deleteFile(String fileName)throws StorageException ;
	public String uploadFile(String fileName, MultipartFile file)throws StorageException ,IOException;
	public String DownloadFile(String fileName) throws StorageException;
	public void MakeBucketPublic();
}

package master.ccm2.shareatsauth.interfaces;

import java.io.IOException;
import java.net.MalformedURLException;
import java.util.concurrent.ExecutionException;

import org.springframework.web.multipart.MultipartFile;

import com.google.firebase.auth.FirebaseAuthException;
import com.google.firebase.auth.FirebaseToken;

import master.ccm2.shareatsauth.exceptions.ErreurManageUserException;
import master.ccm2.shareatsauth.models.User;

/**
 * définit les méthodes pour créer,mettre à jour , supprimer ,creer un token ,verifier un token
 */
public interface ManageUserInterface {
    //ajoute un utilisateur à la base de donnée
    public void create(User newUser) throws FirebaseAuthException, ErreurManageUserException;
    //met à jour un utilisateur
    public User update(FirebaseToken userInToken, User user) throws FirebaseAuthException, ErreurManageUserException;
    //supprime un user
    public void delete(String id) throws FirebaseAuthException;
    //récupère un user par son email
    public User getUserByEmail(String email) throws FirebaseAuthException, InterruptedException, ExecutionException;
    //recupère un user par son uid
    public User getByUid(String id) throws FirebaseAuthException, InterruptedException, ExecutionException;
	//check si la photo de profil correspond à la photo envoyer
	public Boolean checkUserPhoto(FirebaseToken userInToken,MultipartFile photoAverifier) throws MalformedURLException, IOException;
}

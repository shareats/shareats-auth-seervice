package master.ccm2.shareatsauth.interfaces;

import java.util.Map;
import java.util.concurrent.ExecutionException;

public interface DataBaseInterface {
	public void set (Object data,String collection,String document);
	public void add(Object data,String collection);
	public void update(Object data,String collection,String document,String field);
	public void delete(String collection,String document);
	public Map<String, Object> get(String collection,String document) throws InterruptedException, ExecutionException;

	
}

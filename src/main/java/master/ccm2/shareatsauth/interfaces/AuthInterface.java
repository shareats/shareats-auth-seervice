package master.ccm2.shareatsauth.interfaces;

import com.google.firebase.auth.FirebaseAuthException;
import com.google.firebase.auth.FirebaseToken;

import master.ccm2.shareatsauth.exceptions.ErreurAuthException;

/**
 * gère l'authentification à l'api en vérifiant le token ou en le supprimant
 * @author oem
 *
 */
public interface AuthInterface {
	public FirebaseToken autentification(String idToken) throws FirebaseAuthException, ErreurAuthException ;
	void revokeToken(String UserUid) throws FirebaseAuthException, ErreurAuthException;
}

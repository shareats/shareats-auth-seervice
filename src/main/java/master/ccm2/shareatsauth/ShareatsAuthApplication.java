package master.ccm2.shareatsauth;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ShareatsAuthApplication {

	public static void main(String[] args) {
		SpringApplication.run(ShareatsAuthApplication.class, args);
	}

}

package master.ccm2.shareatsauth.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthException;
import com.google.firebase.auth.FirebaseToken;

import master.ccm2.shareatsauth.exceptions.ErreurAuthException;
import master.ccm2.shareatsauth.exceptions.ErreurManageUserException;
import master.ccm2.shareatsauth.interfaces.AuthInterface;
import master.ccm2.shareatsauth.interfaces.ManageUserInterface;
import master.ccm2.shareatsauth.messages.ErrorMessage;
import master.ccm2.shareatsauth.models.User;

@Service
public class FirebaseAuthService implements AuthInterface {
    @Autowired
    ManageUserInterface manageUserService;
    
    /**
     * vérifie que l'idToken renvoyer par le client est correct
     * le boolean à true permet de vérifier que le token n'est pas revoker
     * @throws ErreurManageUserException 
	 * @throws FirebaseAuthException
     * @return FirebaseToken renvoie le token contenant les infos du user
     * @throws ErreurAuthException 
     */
	@Override
	public FirebaseToken autentification(String idToken) throws  ErreurAuthException {
		try {
			return FirebaseAuth.getInstance().verifyIdToken(idToken,true);
		} catch (FirebaseAuthException e) {
			throw  new ErreurAuthException(ErrorMessage.TOKEN_NOT_VALID);
		}
	}
	
	/**
	 * Supprime le token assosier à l'utilisateur 
	 * @param UserUid l'uid du user 
	 * @throws ErreurManageUserException 
	 * @throws ErreurAuthException 
	 * @throws FirebaseAuthException 
	 */
	@Override
	public void revokeToken(String UserUid) throws ErreurAuthException  {
		 try {
			FirebaseAuth.getInstance().revokeRefreshTokens(UserUid);
		} catch (FirebaseAuthException e) {
			throw  new ErreurAuthException(ErrorMessage.TOKEN_NOT_VALID);
		}
	}
	
	
	/**
	 * gère les erreurs pour authentifier un user
	 * @param userIndd le user enregristré en bdd
	 * @param user les infos envoyés par le formulaire
	 * @throws ErreurManageUserException 
	 */
	public void manageUserLogin(User user) throws ErreurManageUserException {

	}


	
	


}

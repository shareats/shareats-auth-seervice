package master.ccm2.shareatsauth.services;

import java.io.IOException;

import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.google.cloud.Identity;
import com.google.cloud.Policy;
import com.google.cloud.storage.Blob;
import com.google.cloud.storage.BlobId;
import com.google.cloud.storage.BlobInfo;
import com.google.cloud.storage.Storage;
import com.google.cloud.storage.StorageException;
import com.google.cloud.storage.StorageRoles;
import com.google.firebase.cloud.StorageClient;

import master.ccm2.shareatsauth.interfaces.BucketStorageInterface;


@Service
public class BucketStorageService implements BucketStorageInterface {


	private Storage bucket ;
	private String bucketName;
	
	public BucketStorageService() {
		super();
		this.bucket = StorageClient.getInstance().bucket().getStorage();
		this.setBucketName(StorageClient.getInstance().bucket().getName());
	}

	/**
	 * Supprime le Fichier du bucket
	 * @return true si le fichier à était supprimé
	 */
	@Override
	public Boolean deleteFile(String fileName) throws StorageException {
		return this.bucket.delete(this.getBucketName(), fileName) ;	
	}
	/**
	 * Télécharge le fichier dans le bucket
	 * @return 
	 * @throws IOException 
	 */
	@Override
	public String uploadFile(String fileName, MultipartFile file) throws StorageException, IOException {
	    BlobId blobId = BlobId.of(this.getBucketName(), fileName);
	    BlobInfo blobInfo = BlobInfo.newBuilder(blobId).build();
	    this.bucket.create(blobInfo,file.getBytes());
	    Blob blob = this.bucket.get(BlobId.of(this.getBucketName(), fileName));
	    return blob.getMediaLink();
	}
	
	@Override
	public String DownloadFile(String fileName) throws StorageException {
	    Blob blob = this.bucket.get(BlobId.of(this.getBucketName(), fileName));
	    return blob.getMediaLink();
	}

	@Override
	public void MakeBucketPublic() {
		Policy originalPolicy = this.bucket.getIamPolicy(this.getBucketName());
	    this.bucket.setIamPolicy(this.getBucketName(),originalPolicy
	    		.toBuilder()
	            .addIdentity(StorageRoles.objectViewer(), Identity.allUsers()) // All users can view
	            .build());
	}

	public String getBucketName() {
		return bucketName;
	}

	public void setBucketName(String bucketName) {
		this.bucketName = bucketName;
	}

	

}

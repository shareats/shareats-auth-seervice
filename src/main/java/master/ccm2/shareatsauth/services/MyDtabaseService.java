package master.ccm2.shareatsauth.services;

import java.util.Map;
import java.util.concurrent.ExecutionException;

import org.springframework.stereotype.Service;

import com.google.api.core.ApiFuture;
import com.google.cloud.firestore.DocumentReference;
import com.google.cloud.firestore.DocumentSnapshot;
import com.google.cloud.firestore.Firestore;
import com.google.cloud.firestore.WriteResult;
import com.google.firebase.cloud.FirestoreClient;

import master.ccm2.shareatsauth.interfaces.DataBaseInterface;

@Service
public class MyDtabaseService implements DataBaseInterface {

	private Firestore db;
	
	public MyDtabaseService() {
		super();
		this.setDb(FirestoreClient.getFirestore());

	}
	

	
	// Add document data with specific document
	@Override
	public void set(Object data, String collection, String document) {
		DocumentReference docRef = this.getDb().collection(collection).document(document);
		docRef.set(data);	
	}
	
	// Add document data with auto-generated id.
	@Override
	public void add(Object data, String collection) {
		 this.getDb().collection(collection).add(data);
	}
	
	
	//update field
	@Override
	public void update(Object data, String collection, String document, String field) {
		DocumentReference docRef = this.getDb().collection(collection).document(document);
		docRef.update(field, data);		
	}
	
	//delete document
	@Override
	public void delete(String collection, String document) {
		this.getDb().collection(collection).document(document).delete();
	}

	//get document
	@Override
	public Map<String, Object> get(String collection, String document) throws InterruptedException, ExecutionException {
	    DocumentReference docRef = this.getDb().collection(collection).document(document);
	    ApiFuture<DocumentSnapshot> future = docRef.get();
	    return future.get().getData();
	}

	public Firestore getDb() {
		return db;
	}

	public void setDb(Firestore db) {
		this.db = db;
	}

}

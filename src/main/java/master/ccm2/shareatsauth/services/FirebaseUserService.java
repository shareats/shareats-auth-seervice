package master.ccm2.shareatsauth.services;

import com.google.auth.oauth2.GoogleCredentials;
import com.google.firebase.FirebaseApp;
import com.google.firebase.FirebaseOptions;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthException;
import com.google.firebase.auth.FirebaseToken;
import com.google.firebase.auth.UserRecord;
import com.google.firebase.database.Transaction.Result;

import master.ccm2.shareatsauth.exceptions.ErreurManageUserException;
import master.ccm2.shareatsauth.interfaces.DataBaseInterface;
import master.ccm2.shareatsauth.interfaces.ManageUserInterface;
import master.ccm2.shareatsauth.messages.ErrorMessage;
import master.ccm2.shareatsauth.models.ResponseApi;
import master.ccm2.shareatsauth.models.User;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.FileSystemResource;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.multipart.MultipartFile;
import org.apache.commons.io.FileUtils;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ExecutionException;

import org.apache.commons.validator.routines.EmailValidator;
@Service
public class FirebaseUserService implements ManageUserInterface {
    @Autowired
    DataBaseInterface databaseService;
    
    @Value("${ip.ia.service}")
    private String ipIaService;
    
    public FirebaseUserService() {
		super();
    	this.initialiseFirebase();
    	System.out.println("env.getProperty(key);"+ipIaService);

	}

	/**
     * ajoute un nouveau user avec son mot de passe et son mail
     * @param newUser les infos de l'utilisateur à ajouter
     * @return UserRecord objet qui contient les infos du user ajouté
     * @throws FirebaseAuthException léve une exception si il est pas possible d'ajouter le user
     * @throws ErreurManageUserException 
     */
    @Override
    public void create(User newUser) throws FirebaseAuthException, ErreurManageUserException {
    	this.manageErrorInscriptionForm(newUser);
        UserRecord.CreateRequest request = new UserRecord.CreateRequest()
                .setEmail(newUser.getEmail())
                .setEmailVerified(false)
                .setPassword(newUser.getPassword())
                .setDisabled(false);
        FirebaseAuth.getInstance().createUser(request);
    }

    /**
     * mise à jour d'un nouveau utilisateur par son uid
     * @param user les infos de l'utilisateur a mettre à jour
     * @throws FirebaseAuthException léve une exception si il est pas possible d'ajouter le user
     * @throws ErreurManageUserException 
     */
    @Override
    public User update(FirebaseToken userInToken,User user) throws FirebaseAuthException, ErreurManageUserException {
    	
    	this.manageUpdateErroForm(userInToken,user);
        UserRecord.UpdateRequest request = new UserRecord.UpdateRequest(userInToken.getUid());
        
        
    	//si la photo de profil est renseigné on remet le booléen isPhotoVerified à false
    	if(user.getUrlPhoto()!=null) {
    		HashMap<String, Object> data =new HashMap<String, Object>();
        	data.put("isPhotoVerified", false);
        	databaseService.set(data,"users",userInToken.getUid());
        	request.setPhotoUrl(user.getUrlPhoto());
    	}
    	
    
        
    	request.setEmail(user.getEmail())
                .setEmailVerified(false)
                .setPassword(user.getPassword())
                .setDisplayName(user.getDisplayName())
                .setDisabled(false)
                .setCustomClaims(user.getClaims());
        FirebaseAuth.getInstance().updateUser(request);
        
        
        return user;

    }

    /**
     * supprime un utilisateur
     * @param uid l'identifiant de l'utilisateur à supprimer
     * @throws FirebaseAuthException
     */
    @Override
    public void delete(String uid) throws FirebaseAuthException {
        FirebaseAuth.getInstance().deleteUser(uid);
        databaseService.delete("users", uid);
    }
    
    

    /**
     *récupère les infos d'un utilisateur
     * @param id l'identifiant de l'utilisateur à récupérer
     * @throws FirebaseAuthException
     * @throws ExecutionException 
     * @throws InterruptedException 
     */
    @Override
    public User getByUid(String id) throws FirebaseAuthException, InterruptedException, ExecutionException {
    	
        UserRecord userRecord = FirebaseAuth.getInstance().getUser(id);
        Map<String, Object> result = databaseService.get("users", userRecord.getUid());
        boolean isPhotoVerified=false;
        if(result!=null) {
            isPhotoVerified=(boolean) result.get("isPhotoVerified");
        }
        return new User(userRecord.getUid(), userRecord.getEmail(), userRecord.getPhoneNumber(), userRecord.getDisplayName(),userRecord.getPhotoUrl(),isPhotoVerified,userRecord.getCustomClaims());
    }
    
    
    /**
     *récupère les infos d'un utilisateur par son mail
     * @param email l'email de l'utilisateur à récupérer
     * @throws FirebaseAuthException
     * @throws ExecutionException 
     * @throws InterruptedException 
     */
    @Override
    public User getUserByEmail(String email) throws FirebaseAuthException, InterruptedException, ExecutionException {
    	
        UserRecord userRecord = FirebaseAuth.getInstance().getUserByEmail(email);
        Map<String, Object> result = databaseService.get("users", userRecord.getUid());
        boolean isPhotoVerified=false;
        if(result!=null) {
            isPhotoVerified=(boolean) result.get("isPhotoVerified");
        }
        return new User(userRecord.getUid(), userRecord.getEmail(), userRecord.getPhoneNumber(), userRecord.getDisplayName(),userRecord.getPhotoUrl(),isPhotoVerified,userRecord.getCustomClaims());
    }
    
    
    public void manageUpdateErroForm(FirebaseToken userInToken,User newUser) throws ErreurManageUserException {
    	
    	if(newUser.getDisplayName().isBlank() || newUser.getDisplayName().isEmpty() || newUser.getDisplayName() == null) {
    		throw new ErreurManageUserException(ErrorMessage.ERREUR_DISPLAYNAME);

    	}
    	if(newUser.getEmail()==null || newUser.getEmail().isEmpty()) {
    		newUser.setEmail(userInToken.getEmail());
    	}
    	
    	if(newUser.getPassword().length()<6 || newUser.getPassword() == null ) {
    		throw new ErreurManageUserException(ErrorMessage.PASSWORD_TO_SHORT);
    	}
 
    	if(!EmailValidator.getInstance().isValid(newUser.getEmail())) {
    		throw new ErreurManageUserException(ErrorMessage.EMAIL_NOT_VALID);
    	}
    	
    }
    
    /**
     * gère les vérifications pour le formulaire d'inscrption  
     * @param newUser objet qui contient les infos du user à ajouter
     * @throws ErreurManageUserException 
     * @throws FirebaseAuthException 
     */
    public void manageErrorInscriptionForm(User newUser) throws ErreurManageUserException {
    	
    	if(newUser.getPassword().length()<6 || newUser.getPassword() == null ) {
    		throw new ErreurManageUserException(ErrorMessage.PASSWORD_TO_SHORT);
    	}
 
    	if(!EmailValidator.getInstance().isValid(newUser.getEmail())) {
    		throw new ErreurManageUserException(ErrorMessage.EMAIL_NOT_VALID);
    	}
    	
    	//vérifie si le mail existe déja
    	UserRecord userRecord;
		try {
			userRecord = FirebaseAuth.getInstance().getUserByEmail(newUser.getEmail());
	    	String emailBdd=userRecord.getEmail();
	    	if(emailBdd.equals(newUser.getEmail())) {
	    		throw new ErreurManageUserException(ErrorMessage.EMAIL_ALREADY_EXIST);
	    	}
		} catch (FirebaseAuthException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

    	
    }
    
    
    /**
     * initialise la connexion à firebase via le fichier shareats-e1dc4-firebase-adminsdk-um0o4-a2e6d85b8e.json
     */
    public void initialiseFirebase() {
    	FileInputStream serviceAccount;

		try {
			serviceAccount = new FileInputStream("shareats-e1dc4-firebase-adminsdk-um0o4-05ee9446b3.json");
			System.out.println("testets"+serviceAccount);
			
			 FirebaseOptions options = new FirebaseOptions.Builder()
					  .setCredentials(GoogleCredentials.fromStream(serviceAccount))
					  .setDatabaseUrl("https://shareats-e1dc4.firebaseio.com")
					  .setStorageBucket("shareats-e1dc4.appspot.com")
					  .build();
			 
            if(FirebaseApp.getApps().isEmpty()) { //<--- check with this line
                FirebaseApp.initializeApp(options);
    			System.out.println("iniatialise");

            }
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

    }
    
    /**
     * vérifier la photo profil correspond bien à la photo envoyé
     * @throws IOException 
     */
	@Override
	public Boolean checkUserPhoto(FirebaseToken userInToken, MultipartFile photoAverifier) throws IOException {
        LinkedMultiValueMap<String, Object> map = new LinkedMultiValueMap<>();
		
        URL url = new URL(userInToken.getPicture());
        File photoProfil = File.createTempFile("photoProfil", ".png");
        File photoAverifierFile = File.createTempFile("photoAverifier", ".png");
        
		FileUtils.copyURLToFile(url, photoProfil);
		FileUtils.copyToFile(photoAverifier.getInputStream(), photoAverifierFile);
		

		//ajoute les données du body de la requtte
		map.add("photoAVerifier",new FileSystemResource(photoProfil));
		map.add("photoProfil", new FileSystemResource(photoAverifierFile));
		//le header de la requette
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.MULTIPART_FORM_DATA);
        
        //l'entité requette
        HttpEntity<LinkedMultiValueMap<String, Object>> requestEntity = new HttpEntity<>(map, headers);
        
        //envoie la requette au backend ia photo
        RestTemplate restTemplate = new RestTemplate();
        ResponseEntity<ResponseApi> result= restTemplate.exchange(ipIaService, HttpMethod.POST, requestEntity, ResponseApi.class);
        
        //supprime les fichiers temporaires
        //photoAverifierFile.deleteOnExit();
        //photoProfil.deleteOnExit();
        
        //si la photo de profil correspond à la photo envoyé on met à jour la collection user
        if((boolean) result.getBody().getResponse()) {
        	HashMap<String, Object> data =new HashMap<String, Object>();
        	data.put("isPhotoVerified", true);
        	databaseService.set(data,"users", userInToken.getUid());
        	return true;
        }else {
        	HashMap<String, Object> data =new HashMap<String, Object>();
        	data.put("isPhotoVerified", false);
        	databaseService.set(data,"users", userInToken.getUid());
        	return false;
        }
	}
}
